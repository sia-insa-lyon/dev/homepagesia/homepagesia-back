## Setup
```bash
pip install -r requirements.txt
python mange.py migrate
python manage.py loaddata example.json
```

## Run
```bash
python manage.py runserver
```
