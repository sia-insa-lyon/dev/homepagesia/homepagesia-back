from django.contrib.auth.models import User, Group
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, GroupSerializer, ProjectSerializer, ProjectMinSerializer, ProjectStateMinSerializer, ProjectCreateSerializer
from .models import Project, ProjectState
from rest_framework import permissions
from rest_framework.decorators import permission_classes
from .permissions import UpdateIfAuthenticated


""" Token get/use (httpie)
http :8000/api/auth/ username="usr" password="pwd"
http :8000/api/users/ "Authorization: Bearer $token"
"""


@permission_classes((permissions.IsAuthenticated,))
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


@permission_classes((permissions.IsAuthenticated,))
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

@permission_classes((UpdateIfAuthenticated, ))
class ProjectViewSet(viewsets.ViewSet):
    """
    Endpoint to view general information about all projects
    """

    def list(self, request):
        queryset = Project.objects.filter(published=True)
        serializer = ProjectMinSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Project.objects.filter(published=True)
        project = get_object_or_404(queryset, pk=pk)
        serializer = ProjectSerializer(project)
        return Response(serializer.data)

    def create(self, request):
        serializer = ProjectCreateSerializer(data=request.data)
        print(request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        project = get_object_or_404(Project.objects.all(), pk=pk)
        serializer = ProjectCreateSerializer(instance=project, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((permissions.IsAuthenticated, ))
class ProjectReviewSet(viewsets.ViewSet):
    """
    Display and review unpublished projects
    """

    def list(self, request):
        queryset = Project.objects.filter(published=False)
        serializer = ProjectSerializer(queryset, many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        # Published is the only field that can be changed using this view
        if "published" in request.data:
            data = {"published": request.data["published"]}
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        project = get_object_or_404(Project.objects.all(), pk=pk)
        serializer = ProjectSerializer(instance=project, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
