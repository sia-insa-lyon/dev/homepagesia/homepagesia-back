from rest_framework import permissions


class UpdateIfAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            if view.action != 'update':
                return True
            else:
                return False
        else:
            return True
