from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Project, ProjectState


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class ProjectStateMinSerializer(serializers.ModelSerializer):
    """
    Minimal information on a project state
    """
    class Meta:
        model = ProjectState
        fields = ['short_state', 'state', 'color']


class ProjectSerializer(serializers.ModelSerializer):
    """
    Serializes all information about a project
    """
    projects_state = ProjectStateMinSerializer()

    class Meta:
        model = Project
        fields = ['pk', 'title', 'projects_state', 'submission_date', 'last_update', 'author', 'author_contact', 'for_who', 'published', 'published_by', 'short_description', 'description', 'team', 'team_contact', 'file']


class ProjectMinSerializer(serializers.ModelSerializer):
    """
    Minimal information about a project
    """
    projects_state = ProjectStateMinSerializer()

    class Meta:
        model = Project
        fields = ['pk', 'title', 'projects_state', 'submission_date', 'published', 'short_description', 'author']


class ProjectCreateSerializer(serializers.ModelSerializer):
    """
    Serializer used for creation and update of a project
    """
    def create(self, validated_data):
        # The following fields can't be set when creating a project
        validated_data['published'] = False
        validated_data['published_by'] = None
        # A mon sens il est possible de créer un projet avec une équipe qui travaille déjà dessus
        # (permet de centraliser la liste des projets infos en cours, SIA ou non)
        # validated_data['team'] = ""
        # validated_data['team_contact'] = ""

        return Project.objects.create(**validated_data)


    class Meta:
        model = Project
        fields = ['title', 'projects_state', 'published', 'published_by', 'short_description', 'description', 'author', 'author_contact', 'for_who', 'team', 'team_contact']