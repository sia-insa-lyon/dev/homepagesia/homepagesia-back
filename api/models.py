from colorful.fields import RGBColorField
from django.contrib.auth.models import User
from django.db import models


class ProjectState(models.Model):
    short_state = models.CharField(max_length=12)
    state = models.CharField(max_length=32)
    description = models.TextField()
    color = RGBColorField()

    def __str__(self):
        return self.short_state + ' - ' + self.state


class Project(models.Model):
    title = models.CharField(max_length=64)
    projects_state = models.ForeignKey(ProjectState, on_delete=models.SET_DEFAULT, default=1)
    submission_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    author = models.CharField(max_length=32)    # TODO : When SSO => use user instead of CharField
    author_contact = models.EmailField()        # Will use user's email l8r
    for_who = models.CharField(max_length=32)   # TODO : same => use entity instead od CharField
    published = models.BooleanField(default=False)
    published_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    short_description = models.CharField(max_length=128)
    description = models.TextField()
    team = models.TextField(blank=True)
    team_contact = models.EmailField(blank=True)
    file = models.FileField(upload_to='projects_files', blank=True)

    def __str__(self):
        return self.title + ' - ' + self.projects_state.short_state
