from django.contrib import admin
from api.models import *

# Register your models here.
admin.site.register(ProjectState)
admin.site.register(Project)
