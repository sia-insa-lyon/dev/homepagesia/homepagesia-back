FROM python:3.7

EXPOSE 8000

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
VOLUME /app/static

RUN chmod +x bash/run-prod.sh
CMD /app/bash/run-prod.sh
